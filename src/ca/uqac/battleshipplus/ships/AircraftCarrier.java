package ca.uqac.battleshipplus.ships;

import ca.uqac.battleshipplus.game.Player;
import ca.uqac.battleshipplus.game.Position;

import java.util.List;
import java.util.Map;

/**
 * Porte-avion.
 */
public class AircraftCarrier extends Ship {
    public static final int length = 5;
    public static final int range = 2;
    public static final String id = "AIRCRAFT_CARRIER";

    public AircraftCarrier(List<Position> pos) {
        positions = pos;
    }

    public String getID() {
        return id;
    }

    public boolean hasSunk() {
        return touchedPositions.size() >= length || touchedPositions.size() > 2;
    }

    public boolean shoot(Player enemyPlayer, Position atPos) {
        if (!isInRange(atPos, range))
            return false;

        List<Position> enemyShipPos, enemyShipTouchedPos;

        for (Map.Entry<String, Ship> entry : enemyPlayer.getShips().entrySet()) {
            enemyShipPos = entry.getValue().positions;
            enemyShipTouchedPos = entry.getValue().touchedPositions;

            if (enemyShipPos.contains(atPos) && !enemyShipTouchedPos.contains(atPos)) {
                enemyShipTouchedPos.add(atPos);
                return true;
            }
        }

        return false;
    }
}
