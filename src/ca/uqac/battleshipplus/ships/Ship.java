package ca.uqac.battleshipplus.ships;

import ca.uqac.battleshipplus.game.Player;
import ca.uqac.battleshipplus.game.Position;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Ship {
    public static final int length = 0;
    public static final int range = 0;
    public static final String id = "SHIP";

    public List<Position> positions;
    public List<Position> touchedPositions;

    public Ship() {
        positions = new ArrayList<>(length);
    }

    public Ship(Position[] pos) {
        positions = Arrays.asList(pos);
    }

    public Ship(List<Position> pos) {
        positions = pos;
    }

    {
        touchedPositions = new ArrayList<>(length);
    }

    public abstract String getID();
    public abstract boolean hasSunk();
    public abstract boolean shoot(Player enemyPlayer, Position atPos);

    public boolean isInRange(Position p, int range) {
        for (Position pos : positions) {
            if (pos.distanceTo(p) <= range)
                return true;
        }

        return false;
    }

    public void move(Position pos) {
        for (Position p : positions) {
            p.X += pos.X;
            p.Y += pos.Y;
        }

        for (Position p : touchedPositions) {
            p.X += pos.X;
            p.Y += pos.Y;
        }
    }
}
