package ca.uqac.battleshipplus.game;

public class Shot {
    public Position position;
    public boolean missed;

    public Shot(Position pos, boolean miss) {
        position = pos;
        missed = miss;
    }
}
