package ca.uqac.battleshipplus.game;

public class Position {
    public int X;
    public int Y;

    public Position(int x, int y) {
        X = x;
        Y = y;
    }

    public Position(String s) {
        String[] pos = s.split(" ");

        if (pos.length < 2)
            throw new IllegalArgumentException("The string format must be x y");

        try {
            X = new Integer(pos[0]);
            Y = new Integer(pos[1]);
        } catch (Exception e) {
            System.out.println(e);
            X = -2;
            Y = -2;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!Position.class.isAssignableFrom(obj.getClass()))
            return false;

        Position pos = (Position) obj;

        return X == pos.X && Y == pos.Y;
    }

    @Override
    public String toString() {
        return "(" + X + "," + Y + ")";
    }

    public int distanceTo(Position p) {
        if (p == null)
            return -1;

        return Math.abs(X - p.X + Y - p.Y);
    }
}
