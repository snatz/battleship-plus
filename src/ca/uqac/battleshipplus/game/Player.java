package ca.uqac.battleshipplus.game;

import ca.uqac.battleshipplus.ships.*;

import java.util.*;


public class Player {
    private Map<String, Ship> ships;
    private List<Shot> shotsFired;

    public Player() {
        ships = new HashMap<>();
        shotsFired = new ArrayList<>();
    }

    public void addShip(String shipId, List<Position> pos) {
        if (ships.containsKey(shipId)) {
            return;
        }

        switch (shipId) {
            case AircraftCarrier.id:
                ships.put(shipId, new AircraftCarrier(pos));
                break;

            case AntiTorpedoBoat.id:
                ships.put(shipId, new AntiTorpedoBoat(pos));
                break;

            case Cruiser.id:
                ships.put(shipId, new Cruiser(pos));
                break;

            case Submarine.id:
                ships.put(shipId, new Submarine(pos));
                break;

            case TorpedoBoat.id:
                ships.put(shipId, new TorpedoBoat(pos));
                break;
        }
    }

    private boolean attack(Player enemyPlayer, Ship withShip, Position atPos) {
        boolean touched = withShip.shoot(enemyPlayer, atPos);
        Shot shot = new Shot(atPos, touched);
        shotsFired.add(shot);

        return touched;
    }

    public boolean attack(Player enemyPlayer, Position atPos) {
        switch (findBoatInRangeOf(atPos)) {
            case AircraftCarrier.id:                return attack(enemyPlayer, getAircraftCarrier(), atPos);
            case AntiTorpedoBoat.id:                return attack(enemyPlayer, getAntiTorpedoBoat(), atPos);
            case Cruiser.id:                        return attack(enemyPlayer, getCruiser(),         atPos);
            case Submarine.id:                      return attack(enemyPlayer, getSubmarine(),       atPos);
            case TorpedoBoat.id:                    return attack(enemyPlayer, getTorpedoBoat(),     atPos);
        }

        return false;
    }

    private String findBoatInRangeOf(Position atPos) {
        int range = 0;

        for (Map.Entry<String, Ship> entry : getShips().entrySet()) {
            switch (entry.getKey()) {
                case AircraftCarrier.id:
                    range = AircraftCarrier.range;
                    break;

                case AntiTorpedoBoat.id:
                    range = AntiTorpedoBoat.range;
                    break;

                case Cruiser.id:
                    range = Cruiser.range;
                    break;

                case Submarine.id:
                    range = Submarine.range;
                    break;

                case TorpedoBoat.id:
                    range = TorpedoBoat.range;
                    break;
            }

            if (entry.getValue().isInRange(atPos, range))
                return entry.getKey();
        }

        return "";
    }

    /**
     * Getters.
     */

    public ArrayList<Ship> getRemainingShips() {
        ArrayList<Ship> listShips = new ArrayList<>();

        for (Map.Entry<String, Ship> i : getShips().entrySet()) {
            if (!i.getValue().hasSunk())
                listShips.add(i.getValue());
        }

        return listShips;
    }

    public List<Shot> getShotsFired() {
        return shotsFired;
    }

    public Map<String, Ship> getShips() {
        return ships;
    }

    public AircraftCarrier getAircraftCarrier() {
        return (AircraftCarrier) ships.get(AircraftCarrier.id);
    }

    public AntiTorpedoBoat getAntiTorpedoBoat() {
        return (AntiTorpedoBoat) ships.get(AntiTorpedoBoat.id);
    }

    public Cruiser getCruiser() {
        return (Cruiser) ships.get(Cruiser.id);
    }

    public Submarine getSubmarine() {
        return (Submarine) ships.get(Submarine.id);
    }

    public TorpedoBoat getTorpedoBoat() {
        return (TorpedoBoat) ships.get(TorpedoBoat.id);
    }
}
