package ca.uqac.battleshipplus.game;

import ca.uqac.battleshipplus.ships.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Game {
    private Player player1;
    private Player player2;
    private int height;
    private int width;
    private boolean againstAi = false;

    public Game(int l, int w) {
        height = l;
        width = w;

        player1 = new Player();
        player2 = new Player();
    }

    /**
     * Initialize the battleship game.
     */
    public void init() {
        initHumanPlayer(player1);

        System.out.println("Do you want to play against the computer? (y/n)");
        String answer = waitPlayerInput("y|n|yes|no", "The answer must be either yes (y) or no (n).");

        againstAi = answer.equals("y") || answer.equals("yes");

        if (againstAi)
            initFakePlayer(player2);
        else
            initHumanPlayer(player2);
    }

    /**
     * Set up an human player.
     * @param player The human player instance.
     */
    private void initHumanPlayer(Player player) {
        List<Position> shipPositions;

        System.out.println("Player, please place your boats.");
        drawPlayerShips(player);

        // Aircraft carrier
        shipPositions = initShipPositions(AircraftCarrier.id, player);
        player.addShip(AircraftCarrier.id, shipPositions);
        drawPlayerShips(player);

        // Anti torpedo boat
        shipPositions = initShipPositions(AntiTorpedoBoat.id, player);
        player.addShip(AntiTorpedoBoat.id, shipPositions);
        drawPlayerShips(player);

        // Cruiser
        shipPositions = initShipPositions(Cruiser.id, player);
        player.addShip(Cruiser.id, shipPositions);
        drawPlayerShips(player);

        // Submarine
        shipPositions = initShipPositions(Submarine.id, player);
        player.addShip(Submarine.id, shipPositions);
        drawPlayerShips(player);

        // Torpedo boat
        shipPositions = initShipPositions(TorpedoBoat.id, player);
        player.addShip(TorpedoBoat.id, shipPositions);
        drawPlayerShips(player);
    }

    /**
     * Set up a fake player.
     * @param player The fake player instance.
     */
    private void initFakePlayer(Player player) {
        List<Position> shipPositions;

        System.out.println("Placing randomly opponent's boats.");

        // Aircraft carrier
        shipPositions = initRandomShipPositions(AircraftCarrier.length, player);
        player.addShip(AircraftCarrier.id, shipPositions);

        // Anti torpedo boat
        shipPositions = initRandomShipPositions(AntiTorpedoBoat.length, player);
        player.addShip(AntiTorpedoBoat.id, shipPositions);

        // Cruiser
        shipPositions = initRandomShipPositions(Cruiser.length, player);
        player.addShip(Cruiser.id, shipPositions);

        // Submarine
        shipPositions = initRandomShipPositions(Submarine.length, player);
        player.addShip(Submarine.id, shipPositions);

        // Torpedo boat
        shipPositions = initRandomShipPositions(TorpedoBoat.length, player);
        player.addShip(TorpedoBoat.id, shipPositions);
    }

    /**
     * Initialize random ship positions.
     * @param length The length of the ship.
     * @param player The player instance.
     * @return A List of Position.
     */
    private List<Position> initRandomShipPositions(int length, Player player) {
        String[] possibleDirections = new String[] { "h", "H", "v", "V" };
        Position pos;
        String direction;

        do {
            pos = new Position(randomInt(0, width - 1), randomInt(0, height - 1));
            direction = possibleDirections[randomInt(0, possibleDirections.length - 1)];
        } while (!checkShipInBoard(pos, direction, length, player));

        return generateShipPositions(pos, direction, length);
    }

    /**
     * Generate an random integer.
     * @param min The minimum.
     * @param max The maximum.
     * @return The generated integer.
     */
    private int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    /**
     * Await a player input in a defined format.
     * @param format The input must be in this format.
     * @param errorMessage An error message in case it is wrongly formatted.
     * @return The string input.
     */
    private String waitPlayerInput(String format, String errorMessage) {
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();

        while (!line.matches(format)) {
            System.out.println(errorMessage);
            line = sc.nextLine();
        }

        return line;
    }

    /**
     * Set up the ship positions, based on the player input and on the board.
     * @param id The ship to be placed on the board.
     * @param player The player instance.
     * @return A List of Position instances of the ship.
     */
    private List<Position> initShipPositions(String id, Player player) {
        int length;
        Position pos;
        String direction;
        boolean err = false;

        switch (id) {
            case AircraftCarrier.id:
                length = AircraftCarrier.length;
                break;

            case AntiTorpedoBoat.id:
                length = AntiTorpedoBoat.length;
                break;

            case Cruiser.id:
                length = Cruiser.length;
                break;

            case Submarine.id:
                length = Submarine.length;
                break;

            case TorpedoBoat.id:
                length = TorpedoBoat.length;
                break;

            default:
                throw new IllegalArgumentException("Ship unknown");
        }

        do {
            if (err)
                System.out.println("Ship is either not on the board or collide with another friendly ship.");

            System.out.println(id + " starting position x y (length: " + length + ").");
            pos = new Position(waitPlayerInput("\\d+\\s+\\d+", "Wrong input: position format must be x y"));
            System.out.println("Which direction? H/V (horizontal / vertical)");
            direction = waitPlayerInput("[HVhv]", "Wrong input: direction must be either H or V");

            err = true;
        } while (!checkShipInBoard(pos, direction, length, player));

        return generateShipPositions(pos, direction, length);
    }

    /**
     * Check if the ship is rightly put on the board.
     * @param pos Starting ship position.
     * @param direction The direction of the ship (horizontal or vertical).
     * @param length The length of the ship.
     * @param player The player instance.
     * @return True if it is rightly put, false otherwise.
     */
    private boolean checkShipInBoard(Position pos, String direction, int length, Player player) {
        boolean inBoard = false;
        List<Position> newShipPositions = generateShipPositions(pos, direction, length);

        if (pos.X < 0 || pos.Y < 0 || pos.X >= width || pos.Y >= height)
            return false;

        if (direction.equals("H") || direction.equals("h")) {
            inBoard = (pos.X + length) < width;
        } else if (direction.equals("V") || direction.equals("v")) {
            inBoard = (pos.Y + length) < height;
        }

        if (!inBoard)
            return false;

        for (Map.Entry<String, Ship> entry : player.getShips().entrySet()) {
            for (Position alreadyOnBoardPosition : entry.getValue().positions) {
                if (newShipPositions.contains(alreadyOnBoardPosition)) {
                    return false;
                }
            }
        }

        return inBoard;
    }

    /**
     * Check if a given ship is in the board, or if it collides with another friendly ship.
     * @param pos The initial position of the ship.
     * @param direction The ship's direction.
     * @param length The ship's length.
     * @param player The player instance.
     * @param id The ship id.
     * @return True if its positions are legal, false otherwise.
     */
    private boolean checkShipInBoard(Position pos, String direction, int length, Player player, String id) {
        boolean inBoard = false;
        List<Position> newShipPositions = generateShipPositions(pos, direction, length);

        if (pos.X < 0 || pos.Y < 0 || pos.X >= width || pos.Y >= height)
            return false;

        if (direction.equals("H") || direction.equals("h")) {
            inBoard = (pos.X + length) < width;
        } else if (direction.equals("V") || direction.equals("v")) {
            inBoard = (pos.Y + length) < height;
        }

        if (!inBoard)
            return false;

        for (Map.Entry<String, Ship> entry : player.getShips().entrySet()) {
            // Doesn't check collisions with the ship given as parameter.
            if (id.equals(entry.getKey()))
                continue;

            for (Position alreadyOnBoardPosition : entry.getValue().positions) {
                if (newShipPositions.contains(alreadyOnBoardPosition)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Generate ship positions, based on the starting position, direction and length.
     * @param pos The starting position.
     * @param direction The direction the ship is facing.
     * @param length The length of the ship.
     * @return A List of Position instances.
     */
    private List<Position> generateShipPositions(Position pos, String direction, int length) {
        List<Position> newShipPositions = new ArrayList<>();

        if (direction.equals("H") || direction.equals("h")) {
            for (int i = 0; i < length; i++) {
                newShipPositions.add(new Position(pos.X + i, pos.Y));
            }
        } else if (direction.equals("V") || direction.equals("v")) {
            for (int i = 0; i < length; i++) {
                newShipPositions.add(new Position(pos.X, pos.Y + i));
            }
        }

        return newShipPositions;
    }

    /**
     * Start the play sequence.
     */
    public void play() {
        do {
            playSequence(getPlayer1(), getPlayer2());
            if(hasPlayerLost(getPlayer2()))
                break;

            if (againstAi)
                playRandomSequence(getPlayer2(), getPlayer1());
            else
                playSequence(getPlayer2(), getPlayer1());

        } while (!hasPlayerLost(getPlayer1()) && !hasPlayerLost(getPlayer2()));

        if (hasPlayerLost(getPlayer1()))
            System.out.println("Player 1 has lost.");
        else
            System.out.println("Player 2 has lost.");
    }

    private boolean canShotThere(Position pos, Player p) {
        for (Map.Entry<String, Ship> i : p.getShips().entrySet()) {
            for (Position shotablePos : i.getValue().positions) {
                if (pos.X == shotablePos.X || pos.Y == shotablePos.Y)
                    return true;
            }
        }

        return false;
    }

    private void playSequence(Player playing, Player opponent) {
        if(playing == player1)
            System.out.print("Player One turn : ");
        else
            System.out.print("Player Two turn : ");
        System.out.println("Shoot! (x y format)");
        this.drawPlayersShots();
        this.drawPlayerShips(playing);
        Position pos;
        do {
            System.out.println("\nYou can only fire on lines and rows where your ships are.\n");
            pos = new Position(waitPlayerInput("\\d+\\s+\\d+",
                    "Wrong input: position format must be x y"));
        }while (!canShotThere(pos, playing));

        boolean touched = playing.attack(opponent, pos);

        if (!touched) {
            System.out.println("Missed at " + pos + ". You can move a boat (2 squares max).");
            displayIdShips(playing);
            System.out.println("Choose the ship you want to move (0 to pass).");
            String idShip = null;
            while(idShip == null)
                idShip = getIdShip(waitPlayerInput("\\d+", "Wrong format : must be a positive integer."), playing);
            if(idShip == "pass")
                return;
            System.out.println(idShip + " selected");
            System.out.println("Enter the move to do (ex: '1 1' or '0 -2') or '0 0' to pass.");
            while(!moveShipPlayer(new Position(waitPlayerInput("-?\\d+\\s+-?\\d+",
                    "Wrong format: must be x y (ex : -1 1).")), idShip, playing));
            this.drawPlayerShips(playing);
        } else {
            System.out.println("Touched at " + pos);
        }
    }

    private void displayIdShips(Player p)
    {
        System.out.println("Ships remaining:");
        int n = 1;
        for(Map.Entry<String, Ship> i : p.getShips().entrySet())
        {
            if(!i.getValue().hasSunk())
                System.out.println(n++ + " : " + i.getKey());
        }
    }
    private String getIdShip(String input, Player p)
    {
        int pos;
        try {
            pos = Integer.parseInt(input);
        } catch (Exception e) {
            return "pass";
        }

        if(pos == 0)
            return "pass";

        int n = 1;
        for(Map.Entry<String, Ship> i : p.getShips().entrySet())
        {
            if(!i.getValue().hasSunk())
                if(n++ == pos)
                    return i.getKey();
        }
        System.out.println(pos + " is a wrong entry.");
        return null;
    }
    private boolean moveShipPlayer(Position pos, String id, Player p)
    {
        if(pos.X == 0 && pos.Y == 0)
            return true;
        if(Math.abs(pos.X) + Math.abs(pos.Y) > 2)
        {
            System.out.println("Invalid input, you can't move more than 2 squares.");
            return false;
        }
        String direction = getDirection(p.getShips().get(id));
        if(Math.max(Math.abs(pos.X), Math.abs(pos.Y)) == 2)
        {
            Position testPos = new Position(pos.X/2, pos.Y/2);
            Position newPos = new Position(p.getShips().get(id).positions.get(0).X + testPos.X,
                    p.getShips().get(id).positions.get(0).Y + testPos.Y);
            if(!checkShipInBoard(newPos, direction, p.getShips().get(id).positions.size(), p, id))
            {
                System.out.println("Invalid : You must go through another ship or the edge of the board to go there.");
                return false;
            }
        }
        Position newPos = new Position(p.getShips().get(id).positions.get(0).X + pos.X,
                p.getShips().get(id).positions.get(0).Y + pos.Y);
        if(checkShipInBoard(newPos, direction, p.getShips().get(id).positions.size(), p, id))
        {
            p.getShips().get(id).move(pos);
            return true;
        }
        System.out.println("Invalid input, the ship can't move there, it collides with another ship " +
                "or is out of the board.");
        return false;
    }

    private void playRandomSequence(Player playing, Player opponent) {
        System.out.println("Shoot! (x y format)");
        this.drawPlayerShips(playing);
        Position pos = new Position(randomInt(0, width - 1), randomInt(0, height - 1));
        boolean touched = playing.attack(opponent, pos);

        if (!touched) {
            System.out.println("Missed at " + pos + ". You can move a boat (2 squares max).");

            //moveShipIA();

        } else {
            System.out.println("Touched at " + pos);
        }
    }
/*
    private void moveShipIA()
    {
        ArrayList<Ship> remainingShips = player2.getShipsRemaining();
        for (int i = 0; i < remainingShips.size(); i++)
        {
            Ship ship = remainingShips.get(i);
            String direction = getDirection(ship);
            Position[] pos = new Position[4];
            pos[0] = new Position(1, 0);
            pos[0] = new Position(-1, 0);
            pos[0] = new Position(0, 1);
            pos[0] = new Position(0, -1);

            for(Position p: pos)
            {
                if(checkShipInBoard(new Position(ship.positions.get(0).X + p.X, ship.positions.get(0).Y + p.Y),
                        direction, ship.positions.size(), player2, ship.getID()))
                {
                    System.out.println("AI moving" + ship.getID() + " to " + p.X + " " + p.Y);
                    ship.move(p);
                    return;
                }
            }
            System.out.println("can't move ship " + ship.getID());
        }
        System.out.println("AI can't move any ship");
    }*/

    private String getDirection(Ship s)
    {
        String str = "H";
        if(s.positions.get(0).X == s.positions.get(1).X)
            str = "V";
        return str;
    }

    public boolean hasPlayerLost(Player player) {
        for (Map.Entry<String, Ship> entry : player.getShips().entrySet()) {
            if (!entry.getValue().hasSunk())
                return false;
        }
        return true;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void drawPlayersShots()
    {
        // recovering all the shots made
        Character[][] shotsMade1 = new Character[width][height];
        for (Iterator<Shot> i = player1.getShotsFired().iterator(); i.hasNext();)
        {
            Shot made = i.next();
            shotsMade1[made.position.X][made.position.Y] = made.missed? 'O':'*';
        }
        Character[][] shotsMade2 = new Character[width][height];
        for (Iterator<Shot> i = player2.getShotsFired().iterator(); i.hasNext();)
        {
            Shot made = i.next();
            shotsMade2[made.position.X][made.position.Y] = made.missed? 'O':'*';
        }
        System.out.print("Player 1 shots : ");
        String padding = "";
        for (int i = 0; i < width; i++)
            padding += ' '; // yumi memory lost
        System.out.print(padding);
        System.out.print(padding);
        System.out.print("Player 2 shots : ");
        System.out.println();

        //PLayer1
        System.out.print(" |");
        for (int i = 0; i < width; i++)
        {
            System.out.print(i);//((char)(i + 65)));
            System.out.print(' ');
        }
        System.out.print(padding);
        //Player2
        System.out.print(" |");
        for (int i = 0; i < width; i++)
        {
            System.out.print(i);//((char)(i + 65)));
            System.out.print(' ');
        }
        System.out.println();

        //Player1
        for (int i = 0; i <= width; i++)
        {
            System.out.print("--");
        }
        System.out.print(padding);
        //Player2
        for (int i = 0; i <= width; i++)
        {
            System.out.print("--");
        }
        System.out.println();

        for (int j = 0; j < height; j++)
        {
            // Player1
            System.out.print(j);
            System.out.print('|');
            for (int i = 0; i < width; i++)
            {
                if(shotsMade1[i][j] != null)
                    System.out.print(shotsMade1[i][j]);
                else
                    System.out.print('_');
                System.out.print(' ');
            }
            System.out.print(padding);
            // PLayer2
            System.out.print(j);
            System.out.print('|');
            for (int i = 0; i < width; i++)
            {
                if(shotsMade2[i][j] != null)
                    System.out.print(shotsMade2[i][j]);
                else
                    System.out.print('_');
                System.out.print(' ');
            }
            System.out.println();
        }

        //Player1
        ArrayList<Ship> shipsSunk1 = new ArrayList<Ship>();
        for(Map.Entry<String, Ship> i : player1.getShips().entrySet())
        {
            Ship ship = i.getValue();
            if(ship.hasSunk())
                shipsSunk1.add(ship);
        }
        //Player2
        ArrayList<Ship> shipsSunk2 = new ArrayList<Ship>();
        for(Map.Entry<String, Ship> i : player2.getShips().entrySet())
        {
            Ship ship = i.getValue();
            if(ship.hasSunk())
                shipsSunk2.add(ship);
        }
        //Player1
        if(!shipsSunk1.isEmpty())
            System.out.print("Lost boats : ");
        else
            System.out.print(padding);

        System.out.print(padding);
        System.out.print(padding);
        //Player2
        if(!shipsSunk2.isEmpty())
        {
            System.out.print("Lost boats : ");
        }
        System.out.println();


        for(int i = 0; i < Math.max(shipsSunk1.size(), shipsSunk2.size()); i++)
        {
            //Player1
            if(i < shipsSunk1.size())
            {
                System.out.print(shipsSunk1.get(i).getID());
                System.out.print(" : ");
                for(int k = 0; k < shipsSunk1.get(i).positions.size(); k++)
                    System.out.print("#");
            }
            else
                System.out.print(padding);
            System.out.print(padding);
            System.out.print(padding);
            //Player2
            if(i < shipsSunk2.size())
            {
                System.out.print(shipsSunk2.get(i).getID());
                System.out.print(" : ");
                for(int k = 0; k < shipsSunk2.get(i).positions.size(); k++)
                    System.out.print("#");
            }
            System.out.println();
        }
    }
    public void drawPlayerShips(Player p)
    {
        // get ships to place on grid
        Character[][] shipsPos = new Character[width][height];
        ArrayList<Ship> sunkShips = new ArrayList<Ship>();
        for(Map.Entry<String, Ship> i : p.getShips().entrySet())
        {
            String id = i.getKey();
            if(i.getValue().hasSunk())
            {
                sunkShips.add(i.getValue());
                //continue;
            }
                for (int k = 0; k < i.getValue().positions.size(); k++)
                {
                    try {
                        shipsPos[i.getValue().positions.get(k).X][i.getValue().positions.get(k).Y] = id.charAt(0);
                    }catch (ArrayIndexOutOfBoundsException e)
                    {
                        System.err.println(i.getKey() + " : " + i.getValue().positions.get(k).X + " " + i.getValue().positions.get(k).Y);
                    }
                }
                for (int k = 0; k < i.getValue().touchedPositions.size(); k++) {
                    try {
                        shipsPos[i.getValue().touchedPositions.get(k).X][i.getValue().touchedPositions.get(k).Y] = '*';
                    }catch (ArrayIndexOutOfBoundsException e)      {
                    System.err.println(i.getKey() + " : " + i.getValue().positions.get(k).X + " " + i.getValue().positions.get(k).Y);
                    }
                }
        }


        if(p == player1)
            System.out.println("First player's ships:");
        else
            System.out.println("Second player's ships:");

        System.out.print(" |");
        for (int i = 0; i < width; i++)
        {
            System.out.print(i);//((char)(i + 65)));
            System.out.print(' ');
        }
        System.out.println();

        for (int i = 0; i <= width; i++)
        {
            System.out.print("--");
        }
        System.out.println();

        for (int j = 0; j < height; j++)
        {
            System.out.print(j);
            System.out.print('|');
            for (int i = 0; i < width; i++)
            {
                if(shipsPos[i][j] != null)
                    System.out.print(shipsPos[i][j]);
                else
                    System.out.print('_');
                System.out.print(' ');
            }
            System.out.println();
        }
        if(!sunkShips.isEmpty())
        {
            System.out.print("Lost boats : ");
            System.out.println();
            for(int i = 0; i < sunkShips.size(); i++)
            {
                System.out.print(sunkShips.get(i).getID());
                System.out.print(" : ");
                for(int k = 0; k < sunkShips.get(i).positions.size(); k++)
                    System.out.print("#");
                System.out.println();
            }
        }
    }
}
