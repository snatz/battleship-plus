package ca.uqac.battleshipplus;

import ca.uqac.battleshipplus.game.Game;
import ca.uqac.battleshipplus.game.Position;

import java.util.Arrays;

public class BattleshipPlus {
    public static void main(String[] args) {
        int length = 10;
        int width = 10;

        Game game = new Game(length, width);

        game.init();
        game.play();
    }
}
